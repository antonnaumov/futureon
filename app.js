const https = require('https');


const params = {
	project: '-MA1551S-odms4rVbJ8A',
	subProject: '-MA1551S-odms4rVbJ8B'
};

if (process.argv.length < 3) {
	console.error("Token is missing, the API call impossible");
	console.usage("node app.js <token>");
	process.exit(1);
}
// Do not hardcode secrets to the code. More interlligent argument parsing libraries could use.
params.token = process.argv[2];
	
https.get(`https://backend.qa.fieldap.com/API/v1.8/${params.project}/subProject/${params.subProject}`, {headers: {'token': params.token}}, (resp) => {
	var data = '';

	resp.on('data', (chunk) => {
		data += chunk;
	});

	resp.on('end', () => {
		buildGraph(JSON.parse(data))
	});

}).on('error', (err) => {
	console.log("Error: " + err.message)
});

// The function is parsing the given field, and find all possible paths on the Graph.
// A main idea of the implementation is to tell isolated nodes, and end vertex first. 
// Then go recursively through each end vertex build possible paths.
function buildGraph(field) {
	const paths = [];
	const endVertex = [];
	for(const key in field.stagedAssets) {
		const asset = field.stagedAssets[key];
		var end = true
		var isolated = true;
		for (const i in asset.connectionsAsFrom) {
			end = false;
			isolated = false;
			break;
		}
		for (const i in asset.connectionsAsTo) {
			isolated = false;
			break;
		}
		// isolated node does not
		if (isolated) {
			paths.push(key);
		}
		if (end) {
			endVertex.push(key);
		}
	}

		endVertex.forEach(id => {
			buildGraphPath(field.stagedAssets, field.connections, id, "").forEach(e => paths.push(e));
		});

	console.log(paths.join("\n"));
}

function buildGraphPath(assets, connections, key, tail) {
	const nextTail = key + (tail == "" ? "" : " > " + tail);
	var res = [];

	var leaf = true;
	if (assets[key] && assets[key].connectionsAsTo) {
		for (const i in assets[key].connectionsAsTo) {
			leaf = false;
			const c = connections[i];
			buildGraphPath(assets, connections, c.from, nextTail).forEach(e => {
				res.push(e);
			});
		}
	}
	if(leaf) {
		res.push(nextTail);
	}

	return res;
}
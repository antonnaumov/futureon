Vanila JS field parser.

I'm considering using vanilla JS because I have no idea about your JS stack, and I do not think it useful to require additional dependencies installation. At the same time, it's a healthy habit to keep secrets away from the code, so the only command line parameter required is the authentication token. 

To run the assignement please run `node app.js <authentication_token>`
